package com.dmitriy.azarenko.workwithretrofit;

import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {

    TextView countryNameText;
    TextView regionText;
    TextView subRegionText;

    final String LOG_TAG = "myLogs";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        countryNameText = (TextView) findViewById(R.id.country_name_id);
        regionText = (TextView) findViewById(R.id.region_id);
        subRegionText = (TextView) findViewById(R.id.post_reg_id);

        final ListView listView = (ListView)findViewById(R.id.listView);

        FileInputStream fis;
        try {
            fis = openFileInput("countries_file");
            ObjectInputStream ois = new ObjectInputStream(fis);
            final ArrayList<Countrys> returnlist = (ArrayList<Countrys>) ois.readObject();
            ois.close();
            listView.setAdapter(new MyAdapter(this, returnlist));
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent mIntent = new Intent(MainActivity.this, SecondActivity.class);
                    Countrys countrys = returnlist.get(position);
                    mIntent.putExtra("key", countrys);
                    startActivity(mIntent);
                }
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }





        Retrofit.getCountries(new Callback<List<Countrys>>() {
            @Override
            public void success(final List<Countrys> countries, Response response) {
                Log.d(LOG_TAG, "получили данные");


                FileOutputStream fos = null;
                try {
                    fos = openFileOutput("countries_file", Context.MODE_PRIVATE);
                    ObjectOutputStream oos = new ObjectOutputStream(fos);
                    oos.writeObject(countries);
                    oos.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }


                listView.setAdapter(new MyAdapter(MainActivity.this, countries));
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent mIntent = new Intent(MainActivity.this,SecondActivity.class);
                        Countrys countrys = countries.get(position);
                        mIntent.putExtra("key", countrys);
                        startActivity(mIntent);
                    }
                });


            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();

            }
        });
    }

    class MyAdapter extends ArrayAdapter<Countrys> {

        public MyAdapter(Context context, List<Countrys> objects) {
            super(context, R.layout.list_item, objects);
        }



        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            View rowView = convertView;
            if (rowView == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                rowView = inflater.inflate(R.layout.list_item, parent, false);
                holder = new ViewHolder();
                holder.textNameofCoutry = (TextView) rowView.findViewById(R.id.country_name_id);
                holder.textRegion = (TextView) rowView.findViewById(R.id.region_id);
                holder.textSubRegion = (TextView) rowView.findViewById(R.id.post_reg_id);
                rowView.setTag(holder);
            } else {
                holder = (ViewHolder) rowView.getTag();
            }

            Countrys countries = getItem(position);
            holder.textNameofCoutry.setText(countries.getName());
            holder.textRegion.setText(countries.getRegion());
            holder.textSubRegion.setText(countries.getSubregion());


            return rowView;
        }

        class ViewHolder {

            public TextView textNameofCoutry;
            public TextView textRegion;
            public TextView textSubRegion;
        }


    }




}
