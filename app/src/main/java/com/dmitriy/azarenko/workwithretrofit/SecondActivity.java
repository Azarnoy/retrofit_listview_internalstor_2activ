package com.dmitriy.azarenko.workwithretrofit;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import java.util.Arrays;

public class SecondActivity extends AppCompatActivity {

    TextView capital_view;
    TextView language_view;
    TextView timezone_view;
    TextView population_view;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        capital_view = (TextView)findViewById(R.id.capital_id);
        language_view = (TextView)findViewById(R.id.languages_id);
        timezone_view = (TextView)findViewById(R.id.timezones_id);
        population_view = (TextView)findViewById(R.id.population_id);

        Countrys countrys = (Countrys) getIntent().getExtras().getSerializable("key");



        capital_view.setText(countrys.getCapital());

        population_view.setText(""+countrys.getPopulation());

        language_view.setText(Arrays.toString(countrys.getLanguages()).replaceAll("\\[|\\]", ""));
        timezone_view.setText(Arrays.toString(countrys.getTimezones()).replaceAll("\\[|\\]", ""));



    }


}
