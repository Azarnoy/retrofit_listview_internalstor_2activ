package com.dmitriy.azarenko.workwithretrofit;

import java.io.Serializable;

/**
 * Created by Дмитрий on 14.02.2016.
 */
public class Countrys implements Serializable {

    String name;
    String capital;
    String region;
    String subregion;
    int population;

    String[] languages;
    String[] timezones;


    public Countrys(String name, String region, String subregion) {
        this.name = name;
        this.region = region;
        this.subregion = subregion;


    }

    public String[] getLanguages() {
        return languages;
    }

    public void setLanguages(String[] languages) {
        this.languages = languages;
    }

    public String[] getTimezones() {
        return timezones;
    }

    public void setTimezones(String[] timezones) {
        this.timezones = timezones;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getSubregion() {
        return subregion;
    }

    public void setSubregion(String subregion) {
        this.subregion = subregion;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }



}
