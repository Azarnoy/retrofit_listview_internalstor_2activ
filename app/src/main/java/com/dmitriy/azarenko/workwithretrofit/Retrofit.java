package com.dmitriy.azarenko.workwithretrofit;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by Дмитрий on 14.02.2016.
 */
public class Retrofit {

    private static final String ENDPOINT = "http://restcountries.eu/rest";
    private static ApiInterface apiInterface;

    interface ApiInterface {
        @GET("/v1/all")
        void getCountries(Callback<List<Countrys>> callback);


    }

    static {
        init();
    }

    private static void init() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }

    public static void getCountries(Callback<List<Countrys>> callback) {
        apiInterface.getCountries(callback);
    }




}
